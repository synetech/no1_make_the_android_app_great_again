package cz.synetech.livedemo.ui.screens

import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import cz.synetech.livedemo.R
import cz.synetech.livedemo.viewmodels.PetitionViewModel
import cz.synetech.livedemo.wizard.NameActivity
import cz.synetech.livedemo.BR
import cz.synetech.livedemo.databinding.ActivityPetitionBinding
import kotlinx.android.synthetic.main.activity_petition.*
import org.koin.android.architecture.ext.viewModel

interface PetitionView {
    fun goToWizardScreen()
    fun goToPersonsScreen()
}

class PetitionActivity : AppCompatActivity(), PetitionView {
    private val viewModel by viewModel<PetitionViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityPetitionBinding = DataBindingUtil.setContentView(this,
                R.layout.activity_petition)
        binding.setVariable(BR.viewModel, viewModel)
        binding.setLifecycleOwner(this)
        viewModel.view = this
        loadPetition()
    }

    private fun loadPetition() {
        wv_petition?.loadUrl(viewModel.getPetitionContentUrl())
    }

    override fun goToWizardScreen() {
        val intent = Intent(this, NameActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun goToPersonsScreen() {
        val intent = Intent(this, PersonsActivity::class.java)
        startActivity(intent)
        finish()
    }
}
