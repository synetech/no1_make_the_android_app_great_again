package cz.synetech.livedemo.ui.screens

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import cz.synetech.livedemo.R
import cz.synetech.livedemo.domain.adapters.PersonsAdapter
import cz.synetech.livedemo.viewmodels.PersonsViewModel
import kotlinx.android.synthetic.main.activity_persons.*
import org.koin.android.architecture.ext.viewModel

class PersonsActivity : AppCompatActivity() {
    private val viewModel by viewModel<PersonsViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_persons)
        setupList()
    }

    private fun setupList() {
        val adapter = PersonsAdapter(viewModel.getData(), this)
        rv_list.setHasFixedSize(true)
        rv_list.layoutManager = LinearLayoutManager(this)
        rv_list.adapter = adapter
    }
}