package cz.synetech.livedemo.wizard

import cz.synetech.livedemo.R
import cz.synetech.livedemo.data.storage.PersonStorage
import cz.synetech.livedemo.data.validator.SurnameValidator
import cz.synetech.livedemo.ui.screens.wizard.WizardActivity
import kotlinx.android.synthetic.main.activity_surname.*
import org.koin.android.ext.android.inject

class SurnameActivity : WizardActivity() {
    override val layoutFile = R.layout.activity_surname
    override val etInput by lazy { et_surname }
    override val bNext by lazy { b_next }
    override val nextActivity = EmailActivity::class.java
    override val validator: SurnameValidator by inject()
    private val storage: PersonStorage by inject()

    override fun loadData(): String = storage.loadSurname()

    override fun saveData(data: String) {
        storage.saveSurname(data)
    }
}