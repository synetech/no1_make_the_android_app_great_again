package cz.synetech.livedemo.ui.viewholders

import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import cz.synetech.livedemo.BR
import cz.synetech.livedemo.data.model.PersonLiveData

class PersonViewHolder(private val binding: ViewDataBinding) :
        RecyclerView.ViewHolder(binding.root) {

    fun bind(person: PersonLiveData) {
        binding.setVariable(BR.person, person)
        binding.executePendingBindings()
    }
}