package cz.synetech.livedemo.wizard

import cz.synetech.livedemo.R
import cz.synetech.livedemo.data.storage.PersonStorage
import cz.synetech.livedemo.data.validator.EmailValidator
import cz.synetech.livedemo.ui.screens.OverviewActivity
import cz.synetech.livedemo.ui.screens.wizard.WizardActivity
import kotlinx.android.synthetic.main.activity_email.*
import org.koin.android.ext.android.inject

class EmailActivity : WizardActivity() {
    override val layoutFile = R.layout.activity_email
    override val etInput by lazy { et_email }
    override val bNext by lazy { b_next }
    override val nextActivity = OverviewActivity::class.java
    override val validator: EmailValidator by inject()
    private val storage: PersonStorage by inject()

    override fun loadData(): String = storage.loadEmail()

    override fun saveData(data: String) {
        storage.saveEmail(data)
    }
}
