package cz.synetech.livedemo.ui.screens.wizard

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import cz.synetech.livedemo.data.validator.StringValidator

abstract class WizardActivity : AppCompatActivity() {
    protected abstract val layoutFile: Int
    protected abstract val etInput: EditText
    protected abstract val bNext: Button
    protected abstract val nextActivity: Class<*>
    protected abstract val validator: StringValidator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutFile)
        setupViewListeners()
        setupData()
    }

    private fun setupViewListeners() {
        etInput.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(text: Editable?) {
                afterInputTextChanged(text?.toString() ?: "")
            }

            override fun beforeTextChanged(text: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(text: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        })
        bNext.setOnClickListener {
            onNextButtonClick()
        }
    }

    private fun afterInputTextChanged(text: String) {
        bNext.isEnabled = validateData(text)
    }

    private fun onNextButtonClick() {
        val data: String = etInput.text.toString()
        if (validateData(data)) {
            saveData(data)
            goToNextScreen()
        }
    }

    private fun setupData() {
        etInput.setText(loadData(), TextView.BufferType.EDITABLE)
    }

    protected abstract fun loadData(): String

    protected abstract fun saveData(data: String)

    private fun validateData(data: String): Boolean {
        return validator.validate(data)
    }

    private fun goToNextScreen() {
        val intent = Intent(this, nextActivity)
        startActivity(intent)
        finish()
    }
}
