package cz.synetech.livedemo.wizard

import cz.synetech.livedemo.R
import cz.synetech.livedemo.data.storage.PersonStorage
import cz.synetech.livedemo.data.validator.NameValidator
import cz.synetech.livedemo.ui.screens.wizard.WizardActivity
import kotlinx.android.synthetic.main.activity_name.*
import org.koin.android.ext.android.inject

class NameActivity : WizardActivity() {
    override val layoutFile = R.layout.activity_name
    override val etInput by lazy { et_name }
    override val bNext by lazy { b_next }
    override val nextActivity = SurnameActivity::class.java
    override val validator: NameValidator by inject()
    private val storage: PersonStorage by inject()

    override fun loadData(): String = storage.loadName()

    override fun saveData(data: String) {
        storage.saveName(data)
    }
}
