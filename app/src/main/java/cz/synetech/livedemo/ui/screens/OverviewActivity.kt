package cz.synetech.livedemo.ui.screens

import android.app.AlertDialog
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import cz.synetech.livedemo.BR
import cz.synetech.livedemo.R
import cz.synetech.livedemo.databinding.ActivityOverviewBinding
import cz.synetech.livedemo.viewmodels.OverviewViewModel
import org.koin.android.architecture.ext.viewModel

interface OverviewView {
    fun showError()
    fun showGreeting(greeting: String)
    fun goToPersonsScreen()
}

class OverviewActivity : AppCompatActivity(), OverviewView {
    private val viewModel by viewModel<OverviewViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityOverviewBinding = DataBindingUtil.setContentView(this, R.layout.activity_overview)
        binding.setVariable(BR.viewModel, viewModel)
        binding.setLifecycleOwner(this)
        viewModel.view = this
    }

    override fun showError() {
        AlertDialog.Builder(this)
                .setTitle("Chyba")
                .setMessage("Nepodařilo se zpracovat požadavek.")
                .setNegativeButton(android.R.string.cancel) { dialog, _ -> dialog?.dismiss() }
                .show()
    }

    override fun showGreeting(greeting: String) {
        Toast.makeText(this, greeting, Toast.LENGTH_LONG).show()
    }

    override fun goToPersonsScreen() {
        val intent = Intent(this, PersonsActivity::class.java)
        startActivity(intent)
        finish()
    }
}
