package cz.synetech.livedemo.di

import cz.synetech.livedemo.Const
import cz.synetech.livedemo.data.api.ApiService
import cz.synetech.livedemo.data.storage.PersonStorage
import cz.synetech.livedemo.data.storage.PersonStorageImpl
import cz.synetech.livedemo.data.storage.SessionStorage
import cz.synetech.livedemo.data.storage.SessionStorageImpl
import cz.synetech.livedemo.data.validator.EmailValidator
import cz.synetech.livedemo.data.validator.NameValidator
import cz.synetech.livedemo.data.validator.SurnameValidator
import cz.synetech.livedemo.domain.interactors.OverviewInteractor
import cz.synetech.livedemo.domain.interactors.OverviewInteractorImpl
import cz.synetech.livedemo.domain.interactors.SessionInteractor
import cz.synetech.livedemo.domain.interactors.SessionInteractorImpl
import cz.synetech.livedemo.viewmodels.OverviewViewModel
import cz.synetech.livedemo.viewmodels.PersonsViewModel
import cz.synetech.livedemo.viewmodels.PetitionViewModel
import org.koin.android.architecture.ext.viewModel
import org.koin.dsl.module.Module
import org.koin.dsl.module.applicationContext
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val mainModule: Module = applicationContext {
    // region ViewModels
    viewModel { PetitionViewModel(get()) }
    viewModel { OverviewViewModel(get(), get(), get()) }
    viewModel { PersonsViewModel() }
    // endregion

    // region Persistence
    bean { SessionInteractorImpl(get()) as SessionInteractor }
    bean { SessionStorageImpl(get()) as SessionStorage }
    bean { PersonStorageImpl(get()) as PersonStorage }
    bean { OverviewInteractorImpl(get()) as OverviewInteractor }
    // endregion

    // region Validators
    bean { NameValidator() }
    bean { SurnameValidator() }
    bean { EmailValidator() }
    // endregion

    // region Network
    bean { (get() as Retrofit).create(ApiService::class.java) as ApiService }
    bean {
        Retrofit.Builder()
                //.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(get())
                .baseUrl(Const.BASE_URL)
                .build() as Retrofit
    }
    bean { GsonConverterFactory.create() as retrofit2.Converter.Factory }
    // endregion
}