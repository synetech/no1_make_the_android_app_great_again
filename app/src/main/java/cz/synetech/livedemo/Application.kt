package cz.synetech.livedemo

import android.app.Application
import cz.synetech.livedemo.di.mainModule
import org.koin.android.ext.android.startKoin

class Application: Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin(this, listOf(mainModule))
    }
}