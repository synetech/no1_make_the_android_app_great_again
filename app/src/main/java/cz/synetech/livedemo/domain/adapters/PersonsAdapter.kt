package cz.synetech.livedemo.domain.adapters

import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.LiveData
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import cz.synetech.livedemo.R
import cz.synetech.livedemo.data.model.PersonLiveData
import cz.synetech.livedemo.databinding.ListItemPersonBinding
import cz.synetech.livedemo.ui.viewholders.PersonViewHolder
import android.arch.lifecycle.Observer

class PersonsAdapter(personsLive: LiveData<List<PersonLiveData>>,
                     private val lifecycleOwner: LifecycleOwner) :
        RecyclerView.Adapter<PersonViewHolder>() {

    private var persons: List<PersonLiveData>? = null

    init {
        persons = personsLive.value
        personsLive.observe(lifecycleOwner, Observer<List<PersonLiveData>> {
            persons = it
            notifyDataSetChanged()
        })
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PersonViewHolder {
        val dataBinding: ListItemPersonBinding =
                DataBindingUtil.inflate(LayoutInflater.from(parent.context),
                        R.layout.list_item_person,
                        parent,
                        false)
        dataBinding.setLifecycleOwner(lifecycleOwner)
        return PersonViewHolder(dataBinding)
    }

    override fun getItemCount(): Int = persons?.size ?: 0

    override fun onBindViewHolder(holder: PersonViewHolder, position: Int) {
        val person = persons?.get(position) ?: PersonLiveData("", "", "")
        holder.bind(person)
    }
}