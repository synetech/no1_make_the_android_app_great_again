package cz.synetech.livedemo.domain.interactors

import cz.synetech.livedemo.data.model.GreetingApiResponseModel
import cz.synetech.livedemo.data.model.PersonApiModel

interface OverviewInteractor {
    fun submitPetition(person: PersonApiModel,
                       callback: (error: Throwable?, result: GreetingApiResponseModel?) -> Unit)
}