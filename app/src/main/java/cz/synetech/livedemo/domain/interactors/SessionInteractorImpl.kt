package cz.synetech.livedemo.domain.interactors

import cz.synetech.livedemo.data.storage.SessionStorage
import java.util.*

class SessionInteractorImpl(private val storage: SessionStorage): SessionInteractor {
    override fun hasUserSigned(): Boolean = (storage.loadUserSignedAt() > 0)

    override fun setUserSigned() {
        storage.saveUserSignedAt(Date().time)
    }

    override fun getUserSignedAt(): Date? {
        val timestamp = storage.loadUserSignedAt()
        if (timestamp == 0L) {
            return null
        } else {
            return Date(timestamp)
        }
    }

}