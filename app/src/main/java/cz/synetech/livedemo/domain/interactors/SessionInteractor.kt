package cz.synetech.livedemo.domain.interactors

import java.util.*

interface SessionInteractor {
    fun hasUserSigned(): Boolean
    fun setUserSigned()
    fun getUserSignedAt(): Date?
}
