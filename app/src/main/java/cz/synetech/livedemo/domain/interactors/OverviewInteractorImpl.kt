package cz.synetech.livedemo.domain.interactors

import cz.synetech.livedemo.data.api.ApiService
import cz.synetech.livedemo.data.model.GreetingApiResponseModel
import cz.synetech.livedemo.data.model.PersonApiModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OverviewInteractorImpl(private val apiService: ApiService) : OverviewInteractor {
    override fun submitPetition(person: PersonApiModel,
                                callback: (error: Throwable?,
                                           result: GreetingApiResponseModel?) -> Unit) {
        apiService.postPetition(person).enqueue(
                object : Callback<GreetingApiResponseModel> {
                    override fun onResponse(call: Call<GreetingApiResponseModel>?,
                                            response: Response<GreetingApiResponseModel>?) {
                        if (response != null && response.isSuccessful && response.body() != null) {
                            val greetingModel = response.body()
                            callback(null, greetingModel)
                        } else {
                            callback(IllegalStateException("Reponse not usable"), null)
                        }
                    }

                    override fun onFailure(call: Call<GreetingApiResponseModel>?, t: Throwable?) {
                        callback(t, null)
                    }
                })
    }

}