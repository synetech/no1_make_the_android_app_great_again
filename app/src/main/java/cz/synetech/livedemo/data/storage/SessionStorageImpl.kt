package cz.synetech.livedemo.data.storage

import android.content.Context
import android.content.SharedPreferences

private const val PREF_FILE_NAME = "app"

private const val PREF_KEY_USER_SIGNED_AT = "userSignedAt"

class SessionStorageImpl(private val context: Context): SessionStorage {
    private val sharedPreferences: SharedPreferences by lazy {
        context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE)
    }
    private val editor: SharedPreferences.Editor by lazy {
        sharedPreferences.edit()
    }

    override fun saveUserSignedAt(time: Long) {
        editor.putLong(PREF_KEY_USER_SIGNED_AT, time).commit()
    }

    override fun loadUserSignedAt(): Long = sharedPreferences.getLong(PREF_KEY_USER_SIGNED_AT, 0)
}