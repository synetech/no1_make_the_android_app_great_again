package cz.synetech.livedemo.data.model

data class PersonApiModel(var name: String, var surname: String, var email: String)