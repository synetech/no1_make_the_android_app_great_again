package cz.synetech.livedemo.data.validator

private const val MIN_NAME_LENGTH = 3

class NameValidator: StringValidator {
    override fun validate(data: String): Boolean = data.trim().length >= MIN_NAME_LENGTH
}