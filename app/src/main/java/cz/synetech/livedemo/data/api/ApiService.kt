package cz.synetech.livedemo.data.api

import cz.synetech.livedemo.data.model.GreetingApiResponseModel
import cz.synetech.livedemo.data.model.PersonApiModel
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface ApiService {

    @POST("/livedemo/")
    fun postPetition(@Body person: PersonApiModel): Call<GreetingApiResponseModel>
}