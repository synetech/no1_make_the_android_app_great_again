package cz.synetech.livedemo.data.validator

private const val MIN_SURNAME_LENGTH = 3

class SurnameValidator: StringValidator {
    override fun validate(data: String): Boolean = data.trim().length >= MIN_SURNAME_LENGTH
}