package cz.synetech.livedemo.data.validator

interface Validator<in T> {
    fun validate(data: T): Boolean
}

typealias StringValidator = Validator<String>