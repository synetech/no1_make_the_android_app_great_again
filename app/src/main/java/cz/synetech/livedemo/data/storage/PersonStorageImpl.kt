package cz.synetech.livedemo.data.storage

import android.content.Context
import android.content.SharedPreferences

private const val PREF_FILE_NAME = "person"

private const val PREF_KEY_EMAIL = "email"
private const val PREF_KEY_NAME = "name"
private const val PREF_KEY_SURNAME = "surname"

class PersonStorageImpl(private val context: Context) : PersonStorage {
    private val sharedPreferences: SharedPreferences by lazy {
        context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE)
    }
    private val editor: SharedPreferences.Editor by lazy {
        sharedPreferences.edit()
    }

    override fun saveName(name: String) {
        editor.putString(PREF_KEY_NAME, name).commit()
    }

    override fun loadName(): String = sharedPreferences.getString(PREF_KEY_NAME, "")

    override fun saveSurname(surname: String) {
        editor.putString(PREF_KEY_SURNAME, surname).commit()
    }

    override fun loadSurname(): String = sharedPreferences.getString(PREF_KEY_SURNAME, "")

    override fun saveEmail(email: String) {
        editor.putString(PREF_KEY_EMAIL, email).commit()
    }

    override fun loadEmail(): String = sharedPreferences.getString(PREF_KEY_EMAIL, "")
}