package cz.synetech.livedemo.data.validator

import android.text.TextUtils

class EmailValidator: StringValidator {
    override fun validate(data: String): Boolean {
        return !TextUtils.isEmpty(data)
                && android.util.Patterns.EMAIL_ADDRESS.matcher(data).matches()
    }
}