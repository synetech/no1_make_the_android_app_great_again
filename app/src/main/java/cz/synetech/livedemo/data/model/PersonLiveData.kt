package cz.synetech.livedemo.data.model

import android.arch.lifecycle.MutableLiveData

class PersonLiveData(initName: String, initSurname: String, initEmail: String) {
    val name = MutableLiveData<String>()
    val surname = MutableLiveData<String>()
    val email = MutableLiveData<String>()

    init {
        name.value = initName
        surname.value = initSurname
        email.value = initEmail
    }
}