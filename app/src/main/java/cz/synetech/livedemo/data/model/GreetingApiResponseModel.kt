package cz.synetech.livedemo.data.model

data class GreetingApiResponseModel(var greeting: String)