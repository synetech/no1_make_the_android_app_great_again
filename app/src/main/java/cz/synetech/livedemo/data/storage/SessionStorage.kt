package cz.synetech.livedemo.data.storage

interface SessionStorage {
    fun saveUserSignedAt(time: Long)
    fun loadUserSignedAt(): Long
}