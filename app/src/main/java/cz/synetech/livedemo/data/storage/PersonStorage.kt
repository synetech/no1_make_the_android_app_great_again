package cz.synetech.livedemo.data.storage

interface PersonStorage {
    fun saveName(name: String)
    fun loadName(): String
    fun saveSurname(surname: String)
    fun loadSurname(): String
    fun saveEmail(email: String)
    fun loadEmail(): String
}