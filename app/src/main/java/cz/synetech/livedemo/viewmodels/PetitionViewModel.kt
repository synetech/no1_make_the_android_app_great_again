package cz.synetech.livedemo.viewmodels

import android.arch.lifecycle.ViewModel
import cz.synetech.livedemo.domain.interactors.SessionInteractor
import cz.synetech.livedemo.ui.screens.PetitionView

private const val PETITION_CONTENT_URL = "file:///android_res/raw/petition.html"

class PetitionViewModel(private val sessionInteractor: SessionInteractor): ViewModel() {
    var view: PetitionView? = null

    fun onSignButtonClick() {
        if (sessionInteractor.hasUserSigned()) {
            view?.goToPersonsScreen()
        } else {
            view?.goToWizardScreen()
        }
    }

    fun getPetitionContentUrl() = PETITION_CONTENT_URL
}