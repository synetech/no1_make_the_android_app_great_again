package cz.synetech.livedemo.viewmodels

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import cz.synetech.livedemo.data.model.PersonLiveData

class PersonsViewModel : ViewModel() {
    private val persons by lazy {
        // TODO Load from API [GET] https://stub.bbeight.synetech.cz/livedemo
        MutableLiveData<List<PersonLiveData>>().apply {
            value = listOf(
                    PersonLiveData("Jan", "Novak", "jan.novak@email.com"),
                    PersonLiveData("Petr", "Novak", "petr.novak@email.com"),
                    PersonLiveData("Jirka", "Novak", "jirka.novak@email.com"),
                    PersonLiveData("Eva", "Novakova", "eva.novakova@email.com"))
        }
    }

    fun getData(): LiveData<List<PersonLiveData>> {
        return persons
    }
}