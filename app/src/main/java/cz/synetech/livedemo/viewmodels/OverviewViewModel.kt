package cz.synetech.livedemo.viewmodels

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.util.Log
import cz.synetech.livedemo.data.model.GreetingApiResponseModel
import cz.synetech.livedemo.data.model.PersonApiModel
import cz.synetech.livedemo.data.storage.PersonStorage
import cz.synetech.livedemo.domain.interactors.OverviewInteractor
import cz.synetech.livedemo.domain.interactors.SessionInteractor
import cz.synetech.livedemo.ui.screens.OverviewView

private const val TAG = "OverviewViewModel"

class OverviewViewModel(
        private val overviewInteractor: OverviewInteractor,
        storage: PersonStorage,
        private val sessionInteractor: SessionInteractor
) : ViewModel() {
    val name = MutableLiveData<String>()
    val surname = MutableLiveData<String>()
    val email = MutableLiveData<String>()
    val agreementChecked = MutableLiveData<Boolean>()
    val loading = MutableLiveData<Boolean>()
    var view: OverviewView? = null

    init {
        name.postValue(storage.loadName())
        surname.postValue(storage.loadSurname())
        email.postValue(storage.loadEmail())
        agreementChecked.postValue(false)
        loading.postValue(false)
    }

    fun onSubmitButtonClick() {
        submitPetition()
    }

    private fun submitPetition() {
        startLoading()
        val model = createPersonModel()
        overviewInteractor.submitPetition(model) { err, result ->
            if (err != null) {
                onRequestFailed(err)
            } else {
                onRequestSucceeded(result)
            }
            stopLoading()
        }
    }

    private fun createPersonModel(): PersonApiModel {
        return PersonApiModel(
                name = name.value ?: "",
                surname = surname.value ?: "",
                email = email.value ?: "")
    }

    private fun startLoading() {
        loading.postValue(true)
    }

    private fun stopLoading() {
        loading.postValue(false)
    }

    private fun onRequestSucceeded(greetingModel: GreetingApiResponseModel?) {
        setUserSigned()
        view?.showGreeting(greetingModel?.greeting ?: "")
        view?.goToPersonsScreen()
    }

    private fun onRequestFailed(throwable: Throwable?) {
        Log.e(TAG, throwable?.localizedMessage, throwable)
        view?.showError()
    }

    private fun setUserSigned() {
        sessionInteractor.setUserSigned()
    }
}