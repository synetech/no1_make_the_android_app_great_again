# [SYNETECH](https://www.synetech.cz/) MeetUp No1 - "Make the Android app great again"

Here is the code for our first developer meetup. The meetup was about good practices in writing mobile application. At the end there was a "live" demo, 
how to transform bad code and no architecture to maintainable, stable and testable app.

This repository contains few commits that reflect increasing quality of code/architecture. First commit is an example of bad code of application with no 
architecture. Every next commit solves a particular problem. The last (newest) commit contains acceptable code, that is ready to be a good start for 
improving and extension of the application.

## What's bad with initial code?
* No file/package structure
* No SOLID principles
* Hardcoded values (no constants)
* Tightly coupled objects
* Unused code
* Reinventing the wheel - no networking libraries
* No single reponsibility
* No dependency inversion dependency injection
* Spaghetti code
* Long methods/objects
* Copy-pasted code
* Dependencies on implementations (not abstractions) 

## Used technologies and architecture
* MVVM architecture with Android Architecture components
* Koin dependency injection framework
* Retrofit2 networking library (with GSON parser)
* Databinding and LiveData

Note that the final app is not perfect. It still need enhancements like:
* add tests
* create ViewModels for WizardActivities
* add RxJava2 (or another reactive programming framework)
* there are no dimmensions, styles and translated strings (since the demo was about code part not UI part)
* documentation
* remove dependency from ViewModel to view (Activity) and use reactive approach (subscribe to ViewModel and listen for changes)

Feel free to write us your comments to [meetup@synetech.cz](mailto:meetup@synetech.cz) :)
